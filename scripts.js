const Modal = {
    open() {
        document.querySelector('.modal-overlay').classList.add('active')   
    },
    close() {
        document.querySelector('.modal-overlay').classList.remove('active')
    }
};

const transactions = [
    {
        id: 1,
        description: 'Salário',
        amount: 326000,
        date: '07/02/2021'
    },
    {
        id: 2,
        description: 'Aluguel',
        amount: -70000,
        date: '10/02/2021'
    },
    {
        id: 1,
        description: 'Condomínio',
        amount: -23300,
        date: '10/02/2021'
    }
];

const Transaction = {
    incomes() {
        // Somar as entradas
    },
    expanses() {
        // somar as saídas
    },
    total() {
        // entradas - saídas
    }
};

const DOM = {
    transactionsContainer: document.querySelector('#data-table tbody'),

    addTransaction(transaction, index) {
        const tr = document.createElement('tr');
        tr.innerHTML = DOM.innerHTMLTransaction(transaction);

        DOM.transactionsContainer.appendChild(tr);
    },
    
    innerHTMLTransaction(transaction) {
        const CSSclass = transaction.amount > 0 ? "income" : "expanse";

        const amount = Utils.formatCurrency(transaction.amount);

        const html = `
        <td class="description">${transaction.description}</td>
        <td class="${CSSclass}">${amount}</td>
        <td class="date">${transaction.date}</td>
        <td>
            <img src="./assets/minus.svg" alt="Remover Transacao">
        </td>`

        return html;
    }
};

const Utils = {
    formatCurrency(value) {
        const signal = Number(value) < 0 ? '-' : '';
    
        value = String(value).replace(/\D/g, "");

        value = Number(value) / 100;

        value = value.toLocaleString("pt-BR", {
            style: "currency",
            currency: "BRL"
        });

        return signal + value;
    }


};

transactions.forEach(function(transaction) {
    DOM.addTransaction(transaction);
});